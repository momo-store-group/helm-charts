# momo-store helm-charts

Все репозитории, относящиеся к momo-store, объединены в [группе](https://gitlab.com/momo-store-group):

- [Репозиторий с фронтом и бэком](https://gitlab.com/momo-store-group/application)
- [Репозиторий с helm-чартами](https://gitlab.com/momo-store-group/helm-charts) - текущий
- [Репозиторий с инфраструктурой](https://gitlab.com/momo-store-group/infrastructure)

## Install

Данные, которые нужно передать для развертывания чарта:

1. Теги разворачиваемых образов - `{backend,frontend}.image.tag`
2. Имя секрета из кластера, в котором сохранены доступы к registry - `imagePullSecrets[0].name`
    - По умолчанию адреса registry - registry репозитория
      [application](https://gitlab.com/momo-store-group/application).
3. Данные ingress фронта (хост, путь для перенаправления в сервис фронта, имя секрета с TLS сертификатом) - `ingress`

```shell
helm repo add momo-store-repo ${HELM_REPO} --username ${HELM_REPO_USER} --password ${HELM_REPO_PASS}
helm repo update

helm upgrade --install --atomic \
  --kubeconfig ${KUBECONFIG} \
  --namespace ${K8S_NAMESPACE} \
  --set backend.image.tag=${CI_COMMIT_SHA} \
  --set backend.imagePullSecrets[0].name=${K8S_DOCKER_REGISTRY_SECRET} \
  --set frontend.image.tag=${CI_COMMIT_SHA} \
  --set frontend.imagePullSecrets[0].name=${K8S_DOCKER_REGISTRY_SECRET} \
  --set frontend.ingress.hosts[0].host=${K8S_INGRESS_HOST} \
  --set frontend.ingress.hosts[0].paths[0].path='/momo-store' \
  --set frontend.ingress.hosts[0].paths[0].pathType='Prefix' \
  --set frontend.ingress.tls[0].hosts[0]=${K8S_INGRESS_HOST} \
  --set frontend.ingress.tls[0].secretName=letsencrypt \
  momo-store momo-store-repo/momo-store
```

## Repo

Шаблоны фронта и бэка сгенерированы `helm create`.

Структура репозитория helm-charts:

```
helm-charts/
├── momo-store/ - чарт momo-store, оборачивающий чарты фронта и бэка 
│   ├── charts/
│   │   ├── backend/ - директория с чартом бэкенда
│   │   │   ├── templates/
│   │   │   │   ├── deployment.yaml
│   │   │   │   ├── _helpers.tpl
│   │   │   │   ├── hpa.yaml
│   │   │   │   └── service.yaml
│   │   │   └── Chart.yaml
│   │   └── frontend/ - директория с чартом фронтенда
│   │       ├── templates/
│   │       │   ├── configmap.yaml
│   │       │   ├── deployment.yaml
│   │       │   ├── _helpers.tpl
│   │       │   ├── hpa.yaml
│   │       │   ├── ingress.yaml
│   │       │   └── service.yaml
│   │       └── Chart.yaml
│   ├── templates/ - директория для шаблонов общего чарта (требуется, чтобы helm lint не выдавал warning)
│   │   └── .gitkeep
│   ├── Chart.yaml
│   ├── .gitlab-ci.yaml - пайплайн для сборки и релиза общего чарта
│   ├── .helmignore
│   └── values.yaml - конфиг с переменными всех вложенных чартов
├── .gitlab-ci.yaml - пайплайн с триггерами
└── README.md
```

## CI

Входные данные - новый коммит в любой ветке.

Результат - чарт проверен линтерами.

### Pipeline

1. Проверить чарт пакетным менеджером - helm lint
2. Проверить чарт на наличие уязвимостей - kube-linter

## CD

Входные данные - новый коммит в ветке main.

Результат - пакет с чартом загружен в helm репозиторий в Nexus.

### Pipeline

1. Собрать пакет с чартом
2. Загрузить пакет в Nexus

**Версия чарта** задается в файле Chart.yaml и не перезаписывается при сборке.

### Description

Переменные с секретами, используемые на этапе развертывания доступны только из protected веток.

Загрузка пакета выполняется без ручного подтверждения => continuous deployment.

- Цена ошибки - перезаписанный чарт из-за неизмененной версии в Chart.yaml
- Решение - перезапустить job, загружающую пакет, из предыдущего коммита в main

Удаление пакетов из репозитория выполняется вручную.

## Workflow

1. Создать feature-ветку
2. Запушить изменения в feature-ветку
3. В пайплайне проверить измененный чарт линтерами
4. Создать MR в main
5. После принятия MR автоматически загрузить новую версию пакета в Nexus

## References

## TODO
